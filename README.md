This simulator lets the reader to simulate the IoT network and re-get the results.

Title: Network Lifetime Maximization for Cellular-Based M2M Networks.

Authors: Amin Azari, Student Member, IEEE, and Guowang Miao, Senior Member, IEEE.

paper: https://www.researchgate.net/profile/Amin_Azari3/publication/321679912_simulator/data/5a2aa5d645851552ae7a6615/simulator.zip

Abstract:
High energy efficiency is critical for enabling massive machine-type communications (MTC) 
over cellular networks. This paper is devoted to energy consumption modeling, battery 
lifetime analysis, lifetime-aware scheduling, and transmit power control for massive MTC
over cellular networks. We consider a realistic energy consumption model for MTC and model
network battery-lifetime. Analytic expressions are derived to demonstrate the impact of scheduling
on both the individual and network battery lifetimes. The derived expressions are subsequently employed
in the uplink scheduling and transmit power control for mixed-priority MTC traffic in order to maximize
the network lifetime. Besides the main solutions, lowcomplexity solutions with limited feedback 
requirement are investigated, and the results are extended to existing LTE networks. In addition,
the energy efficiency, spectral efficiency, and network lifetime tradeoffs in resource provisioning
and scheduling for MTC over cellular networks are investigated. The simulation results show that the proposed
solutions can provide substantial network lifetime improvement and network maintenance cost reduction in
comparison with the existing scheduling schemes.
 